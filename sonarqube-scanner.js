const scanner = require('sonarqube-scanner')

scanner(
  {
    serverUrl: 'https://sonarqube.businessengineers.de',
    token: 'sqa_f2a39be8c005d1d8952ddfa1dee1208af6152e81',
    options: {
      'sonar.projectKey': 'instants-frontend',
      'sonar.projectName': 'Instants Frontend ',
      'sonar.projectDescription': 'Frontend for the Instants project',
      'sonar.sources': 'assets/styles, components, layouts, pages'
    }
  },
  () => process.exit()
)
