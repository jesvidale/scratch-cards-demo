libraries {
    docker {
        build_frontend_container {
            image_tag = 'pre'
        }
    }
}

keywords {
    snapshotBuildBranch = 'pre'
}
