const between = (x: number, min: number, max: number):boolean => {
  return x >= min && x <= max
}

export const validations = {
  isNotEmpty: (input: any) => {
    return !!input
  },
  types: {
    isAlphaNumeric: (input: string) => {
      const regEx = /^[a-zA-Z0-9 ]*$/
      return input.match(regEx)
    },
    isInteger: (input: string) => {
      return Number.isInteger(parseInt(input))
    },
    isNumber: (input: string) => {
      return !isNaN(parseInt(input))
    },
    isDate: (input: string) => {
      const regEx = /^\d{2}[./-]\d{2}[./-]\d{4}$/
      return input.match(regEx)
    },
    isDateRange: (input: { from: string; to: string }) => {
      const isValidFrom = !!input && ('from' in input) && !!input.from && validations.types.isDate(input.from)
      const isValidTo = !!input && ('to' in input) && !!input.to && validations.types.isDate(input.to)
      return isValidFrom && isValidTo
    },
    isIpV4Address: (input: string) => {
      const regEx = /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/
      return input && input.match(regEx)
    },
    isEmail: (input: string) => {
      const regEx = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/
      return input && input.match(regEx)
    }
  },
  compare: {
    isNumberGreaterThan: (input: string, amount: number) => {
      return parseInt(input) > amount
    },
    isNumberLowerThan: (input: string, amount: number) => {
      return parseInt(input) < amount
    },
    isNumberInRange: (input: number, range: [number, number]) => {
      return between(input, ...range)
    },
    isDateGreaterThan: (input:string, minDate: string) => {
      return date.sort(input, minDate) > 0
    },
    isDateEqualOrGreaterThan: (input:string, minDate: string) => {
      return date.sort(input, minDate) > -1
    },
    isDateEqualOrLowerThan: (input:string, maxDate: string) => {
      return date.sort(input, maxDate) < 1
    }
  }
}
