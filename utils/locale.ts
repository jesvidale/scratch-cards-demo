export const toLocaleNumber = (value: number) => {
  return typeof value === 'number' ? (Math.ceil(Number(value) * 100) / 100).toLocaleString('de') : '-'
}

export const toLocalePrice = (value: number) => {
  return toLocaleNumber(value) + ' €'
}
