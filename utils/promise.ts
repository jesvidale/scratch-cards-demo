export const createPromise = (arg: unknown) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(arg)
    }, 0)
  })
}
