const retrieveObjValues = (nestedObject: Object) => {
  const result = []
  for (const key in nestedObject) {
    if (typeof nestedObject[key] === 'object') {
      for (const nestedKey in nestedObject[key]) {
        result.push(nestedObject[key][nestedKey])
      }
    } else {
      result.push(nestedObject[key])
    }
  }
  return result
}

export const objectHasAllPropsDefined = (obj: Object) => {
  const objValues = retrieveObjValues(obj)
  return !objValues.some(x => x === null || x === '' || x === undefined)
}

export const sortArrayOfObjects = (arr: string[] | number[], objKey: string) => {
  return arr.sort((a, b) => {
    const parseItem = (item: { [x: string]: any }) => {
      if (typeof item === 'undefined') { return item }
      if (typeof item[objKey] === 'string') { return item[objKey].toLowerCase() }
      return item[objKey]
    }
    const itemA = parseItem(a)
    const itemB = parseItem(b)
    if (itemB === undefined || itemA > itemB) {
      return 1
    }
    if (itemA === undefined || itemB > itemA) {
      return -1
    }
    return 0
  })
}
