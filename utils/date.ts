export const date = {
  getTodayTZ: () => {
    const today = new Date()
    return date.parseISOtoDDMMYYYY(today.toISOString())
  },
  getTodayISOslash: () => {
    const today = new Date()
    const tomorrow = today.toISOString().split('T')[0]
    return tomorrow.split('-').join('/')
  },
  parseDDMMYYYYtoISO: (date: string) => {
    return date.split('/').reverse().join('-') + 'T00:00:00Z'
  },
  parseISOtoDDMMYYYY: (date: string) => {
    const res = date.split('T')[0]
    return res.split('-').reverse().join('/')
  },
  parseDashToSlash: (date:string) => {
    return !!date && date.split('-').reverse().join('/')
  },
  parseSlashToDash: (date:string) => {
    return !!date && date.split('/').reverse().join('-')
  },
  toMilliseconds: (date:string) => { // @input date: dd/mm/yyyy
    if (date === undefined || date === null) { return 0 }
    const dateEntries = date.split('/')
    const newDate = `${dateEntries[1]}/${dateEntries[0]}/${dateEntries[2]}`
    return (new Date(newDate)).getTime()
  },
  sort: (a: string, b: string) => { // @input date: dd/mm/yyyy
    const aToMs = date.toMilliseconds(a)
    const bToMs = date.toMilliseconds(b)
    if (aToMs > bToMs) { return 1 }
    if (aToMs < bToMs) { return -1 }
    return 0
  }
}
