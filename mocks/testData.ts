// TODO hardcoded lists of dummy data in services :)
import { warehouseDefinition } from '~/models/warehouse'
import { pickAndPackLineDefinition } from '~/models/pickAndPackLine'
import { orderDefinition } from '~/models/order'
import { retailerDefinition, wheelOfCallAction } from '~/models/retailer'

export const testCouriers = ['DHL', 'UPS']
export const testAreas = ['NORTH', 'SOUTH-WEST']

let testRetailers: Array<retailerDefinition> = [
  {
    name: 'Ronny Retailer',
    address: 'Baker Street 221B',
    retailerNo: '4711',
    id: '1',
    courier: testCouriers[0],
    area: testAreas[0]
  },
  {
    name: 'Richard Retailer',
    address: 'Oxford Street 12',
    retailerNo: '9988',
    id: '2',
    courier: testCouriers[1],
    area: testAreas[1]
  }
]

export function getRetailers () {
  return testRetailers
}

export function addRetailer (retailer: retailerDefinition) {
  testRetailers.push(retailer)
}

export function replaceRetailer (id: number, retailer: retailerDefinition) {
  testRetailers = testRetailers.map((r:any) => {
    if (r.id === id) {
      return retailer
    }
    return r
  })
}

export const testOrdersList: Array<orderDefinition> = testRetailers.flatMap((r, idx) => {
  return [
    { id: idx * 10, retailer: r.name, item: 'Book Game 1', quantity: getRandomNum(1, 5) },
    { id: idx * 100, retailer: r.name, item: 'Merchandise: Pencils', quantity: getRandomNum(1, 5) }
  ]
})

export const testWarehouses: Array<warehouseDefinition> = [
  { id: '1234', name: 'Warehouse A', address: 'Example Street 42', noOfPickAndPack: 2 },
  { id: '9876', name: 'Warehouse B', address: 'Different Street 12', noOfPickAndPack: 1 }
]

export const testPickAndPacksLines: Array<pickAndPackLineDefinition> = [
  {
    id: '1',
    name: 'line 1',
    warehouse: testWarehouses[0].name,
    labelPrinter: '192.168.1.1',
    manifestPrinter: '192.168.1.2',
    stationIp: '192.168.2.2'
  },
  {
    id: '2',
    name: 'line 2',
    warehouse: testWarehouses[0].name,
    labelPrinter: '192.168.1.1',
    manifestPrinter: '192.168.1.2',
    stationIp: '192.168.2.2'
  }
]

export const testWheelOfCallsList: Array<wheelOfCallAction> = testRetailers.map((r, idx) => {
  const dueDate: Date = new Date()
  dueDate.setDate(getRandomNum(1, 25))
  dueDate.setMonth(dueDate.getMonth() + 1)
  return { id: idx, retailer: r.name, phone: '+49 1234 567', dueDate: dueDate.toDateString() }
})

export const testPicks = testOrdersList.map((o, idx) => {
  const retailer = testRetailers.find(r => r.name === o.retailer)
  return { id: idx, retailer: o.retailer, item: o.item, quantity: 2, courier: retailer?.courier, area: retailer?.area }
})

function getRandomNum (min: number, max: number): number {
  return Math.random() * (max - min) + min
}
