const gamesListMock = [{
  gameId: '219d3fc2',
  status: 'CREATED',
  name: 'Test 04',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '232',
  startDate: '2023-12-01',
  numberOfTickets: 10000000,
  creationDate: '2023-11-03T14:13:23.291751Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'a6f92a94',
  status: 'CREATED',
  name: 'acb game 80',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '080',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T11:33:24.275598Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '162e21ab',
  status: 'CREATED',
  name: 'acb game 81',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '081',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T11:39:41.472648Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '78d897c3',
  status: 'CREATED',
  name: 'acb game 82',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '082',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T11:41:26.190711Z',
  creationUser: 'system',
  haveFilesProcessing: true
},
{
  gameId: 'ef35910a',
  status: 'CREATED',
  name: 'acb game 83',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '083',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T11:56:38.012344Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'dcdc7323',
  status: 'CREATED',
  name: 'acb game 84',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '084',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T12:10:00.922411Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'c70be0b7',
  status: 'CREATED',
  name: 'acb game 85',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '085',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T13:11:05.947115Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '98cd5f2c',
  status: 'CREATED',
  name: 'acb game 86',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '086',
  startDate: '2023-12-10',
  numberOfTickets: 5000000,
  creationDate: '2023-08-23T14:13:00.442417Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '49f4e457',
  status: 'CREATED',
  name: 'sp23 test game',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '785548',
  startDate: '2023-08-19',
  numberOfTickets: 100000,
  creationDate: '2023-08-18T08:33:41.498159Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '837177dc',
  status: 'LOADED',
  name: 'game integration 1',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '123',
  startDate: '2023-10-31',
  numberOfTickets: 100000,
  creationDate: '2023-10-25T09:23:24.949753Z',
  creationUser: 'system',
  haveFilesProcessing: true
},
{
  gameId: '57324dbb',
  status: 'ACTIVE',
  name: 'game test 010 david test',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '44567',
  startDate: '2023-07-27',
  numberOfTickets: 4567890,
  creationDate: '2023-07-20T08:33:22.020823Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'bb1d5c5f',
  status: 'ACTIVE',
  name: 'test sp23 ',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '332551',
  startDate: '2023-08-29',
  numberOfTickets: 100000,
  creationDate: '2023-08-16T07:56:22.907325Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '6846fbcf',
  status: 'ACTIVE',
  name: 'regression sp22 test 1',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '454545',
  startDate: '2023-10-01',
  numberOfTickets: 10000000,
  creationDate: '2023-07-21T09:16:46.742002Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '6768f7dc',
  status: 'ACTIVE',
  name: 'regression sp22 test 2',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '4545456',
  startDate: '2023-10-01',
  numberOfTickets: 10000000,
  creationDate: '2023-07-21T09:53:21.878077Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '761aca28',
  status: 'ACTIVE',
  name: 'regression sp22 test 3',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '7987654687965',
  startDate: '2023-07-26',
  numberOfTickets: 1000000,
  creationDate: '2023-07-21T10:11:34.973281Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'ecf49fe8',
  status: 'ACTIVE',
  name: 'regression sp22 test4',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '658479687',
  startDate: '2023-07-24',
  numberOfTickets: 10000000,
  creationDate: '2023-07-21T10:12:34.398823Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'bb1d8166',
  status: 'ACTIVE',
  name: 'regression sp22',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '001',
  startDate: '2023-10-01',
  numberOfTickets: 6000,
  creationDate: '2023-07-18T08:52:47.099345Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '1dbda986',
  status: 'ENDED',
  name: 'game test 005',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '34567',
  startDate: '2023-07-25',
  numberOfTickets: 3456,
  creationDate: '2023-07-19T09:11:04.968775Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'e02e7eb8',
  status: 'ENDED',
  name: 'game test 007',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '3456',
  startDate: '2023-07-27',
  numberOfTickets: 4567,
  creationDate: '2023-07-20T08:27:09.341241Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '27fcdf53',
  status: 'ENDED',
  name: 'regression sp22',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '00',
  startDate: '2023-10-01',
  numberOfTickets: 6000,
  creationDate: '2023-07-18T08:52:22.509414Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'e810106d',
  status: 'ENDED',
  name: 'Champions of Instants',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '122',
  startDate: '2023-10-02',
  numberOfTickets: 1000000,
  creationDate: '2023-09-28T11:59:04.699523Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '38aa9d95',
  status: 'ENDED',
  name: 'Champions of Instants 2',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '201',
  startDate: '2023-09-30',
  numberOfTickets: 2000000,
  creationDate: '2023-09-28T12:10:04.974356Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '7e00d3a3',
  status: 'CLOSED',
  name: 'game test 001',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '002',
  startDate: '2023-10-01',
  numberOfTickets: 6000,
  creationDate: '2023-07-19T08:48:50.011804Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '15812916',
  status: 'CLOSED',
  name: 'regression sp22',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '004',
  startDate: '2023-10-10',
  numberOfTickets: 100000,
  creationDate: '2023-08-03T08:53:19.246159Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '474c636f',
  status: 'CLOSED',
  name: 'regression 3',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '4544',
  startDate: '2023-07-30',
  numberOfTickets: 100000,
  creationDate: '2023-07-25T08:33:19.110317Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'e4c62f76',
  status: 'CLOSED',
  name: 'game test 002',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '567',
  startDate: '2023-07-27',
  numberOfTickets: 456561,
  creationDate: '2023-07-19T09:08:06.431304Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '20a09c0a',
  status: 'CLOSED',
  name: 'game test 003',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '56789',
  startDate: '2023-07-27',
  numberOfTickets: 123534,
  creationDate: '2023-07-19T09:08:42.716876Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '24dda10f',
  status: 'CLOSED',
  name: 'regression',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '45246',
  startDate: '2023-07-26',
  numberOfTickets: 100000,
  creationDate: '2023-07-25T23:26:42.320167Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '4f5f5a78',
  status: 'CLOSED',
  name: 'regression1',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '4524622',
  startDate: '2023-07-26',
  numberOfTickets: 100000,
  creationDate: '2023-07-25T23:39:17.654836Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'cfbad63e',
  status: 'CLOSED',
  name: 'game test 006',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '4567',
  startDate: '2023-07-27',
  numberOfTickets: 5467,
  creationDate: '2023-07-20T08:23:29.281522Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '5754bb10',
  status: 'CLOSED',
  name: 'regresion sp22 present date',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '009',
  startDate: '2023-08-04',
  numberOfTickets: 100000,
  creationDate: '2023-08-04T05:13:09.650620Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '986b45e4',
  status: 'CLOSED',
  name: 'test game one',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '00888',
  startDate: '2023-08-07',
  numberOfTickets: 100000,
  creationDate: '2023-08-02T08:35:20.834690Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '00891594',
  status: 'CLOSED',
  name: 'test game regression sp22',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '12445',
  startDate: '2023-08-22',
  numberOfTickets: 100000,
  creationDate: '2023-08-04T07:46:28.237080Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: '3175b9cd',
  status: 'CLOSED',
  name: 'regression sp22',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '522',
  startDate: '2023-08-30',
  numberOfTickets: 100000,
  creationDate: '2023-08-02T05:52:38.521739Z',
  creationUser: 'system',
  haveFilesProcessing: false
},
{
  gameId: 'b18172bb',
  status: 'CLOSED',
  name: 'game test 004',
  lotteryName: 'Lottery Client Default',
  lotteryNumber: '4567890',
  startDate: '2023-07-27',
  numberOfTickets: 567890,
  creationDate: '2023-07-19T09:10:06.801054Z',
  creationUser: 'system',
  haveFilesProcessing: false
}]

export const getGamesListMock = (filters: string[]) => {
  if (!filters.length) {
    return gamesListMock
  }
  return gamesListMock.filter((game: { status: string}) => {
    return filters.includes(game.status)
  }
  )
}

export const getGameMock = (gameId: string) => gamesListMock.find(game => game.gameId === gameId)

export const gameDefinitionMock = {
  numberOfPools: 5,
  numberOfTicketsByPool: 100000,
  numberOfPacksByCarton: 100,
  numberOfTicketsByPack: 100,
  packWeight: 0.65,
  packUnitOfWeight: 'KG',
  ticketPrice: 20,
  percentageDedicatedToPrizes: 35,
  gameSize: 400000,
  prizeFund: 1400000,
  odds: '1:2.86'
}

export const gamePrizeStructureMock = [
  {
    prizeName: 'PZ-10K',
    prizeAmount: 10000,
    totalOfWinners: 1,
    category: 'HIGH',
    tierCode: 'TYN',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 10000,
    tier: 1,
    prizePerPack: 0.00025
  },
  {
    prizeName: 'PZ-5K',
    prizeAmount: 5000,
    totalOfWinners: 2,
    category: 'HIGH',
    tierCode: 'SEV',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 10000,
    tier: 2,
    prizePerPack: 0.0005
  },
  {
    prizeName: 'PZ-2K',
    totalOfWinners: 10,
    category: 'HIGH',
    tierCode: 'MEZ',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 20000,
    tier: 3,
    prizePerPack: 0.0025
  },
  {
    prizeName: 'PZ-1K',
    prizeAmount: 1000,
    totalOfWinners: 20,
    category: 'HIGH',
    tierCode: 'JRO',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 20000,
    tier: 4,
    prizePerPack: 0.005
  },
  {
    prizeName: 'PZ-500',
    prizeAmount: 500,
    totalOfWinners: 150,
    category: 'MID',
    tierCode: 'SSZ',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 75000,
    tier: 5,
    prizePerPack: 0.0375
  },
  {
    prizeName: 'PZ-200',
    prizeAmount: 200,
    totalOfWinners: 300,
    category: 'MID',
    tierCode: 'SSW',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 60000,
    tier: 6,
    prizePerPack: 0.075
  },
  {
    prizeName: 'PZ-100',
    prizeAmount: 100,
    totalOfWinners: 800,
    category: 'MID',
    tierCode: 'UPS',
    prizeType: 'MERCHANDISE',
    totalPrizeAmount: 80000,
    tier: 7,
    prizePerPack: 0.2
  },
  {
    prizeName: 'PZ-80',
    prizeAmount: 80,
    totalOfWinners: 2000,
    category: 'LOW',
    tierCode: 'NPT',
    prizeType: 'CASH',
    totalPrizeAmount: 160000,
    tier: 8,
    prizePerPack: 0.5
  },
  {
    prizeName: 'PZ-60',
    prizeAmount: 60,
    totalOfWinners: 5000,
    category: 'LOW',
    tierCode: 'NPE',
    prizeType: 'CASH',
    totalPrizeAmount: 300000,
    tier: 9,
    prizePerPack: 1.25
  },
  {
    prizeName: 'PZ-40',
    prizeAmount: 40,
    totalOfWinners: 6000,
    category: 'LOW',
    tierCode: 'PRY',
    prizeType: 'CASH',
    totalPrizeAmount: 240000,
    tier: 10,
    prizePerPack: 1.5
  },
  {
    prizeName: 'PZ-20',
    prizeAmount: 20,
    totalOfWinners: 10000,
    category: 'LOW',
    tierCode: 'OWQ',
    prizeType: 'CASH',
    totalPrizeAmount: 200000,
    tier: 11,
    prizePerPack: 2.5
  },
  {
    prizeName: 'PZ-10',
    prizeAmount: 10,
    totalOfWinners: 22500,
    category: 'LOW',
    tierCode: 'SNW',
    prizeType: 'CASH',
    totalPrizeAmount: 225000,
    tier: 12,
    prizePerPack: 5.625
  }
]

export const gameGltpMock = [
  {
    nameOfPrize: 'PZ-10',
    pattern: 'A',
    quantity: 15,
    prizeAmount: 150
  },
  {
    nameOfPrize: 'PZ-20',
    pattern: 'A',
    quantity: 10,
    prizeAmount: 200
  },
  {
    nameOfPrize: 'PZ-30',
    pattern: 'A',
    quantity: 5,
    prizeAmount: 150
  },
  {
    nameOfPrize: 'PZ-50',
    pattern: 'A',
    quantity: 2,
    prizeAmount: 100
  },
  {
    nameOfPrize: 'PZ-10',
    pattern: 'B',
    quantity: 14,
    prizeAmount: 140
  },
  {
    nameOfPrize: 'PZ-20',
    pattern: 'B',
    quantity: 11,
    prizeAmount: 220
  },
  {
    nameOfPrize: 'PZ-30',
    pattern: 'B',
    quantity: 4,
    prizeAmount: 120
  },
  {
    nameOfPrize: 'PZ-50',
    pattern: 'B',
    quantity: 2,
    prizeAmount: 100
  },
  {
    nameOfPrize: 'PZ-10',
    pattern: 'C',
    quantity: 18,
    prizeAmount: 180
  },
  {
    nameOfPrize: 'PZ-20',
    pattern: 'C',
    quantity: 8,
    prizeAmount: 160
  },
  {
    nameOfPrize: 'PZ-30',
    pattern: 'C',
    quantity: 8,
    prizeAmount: 240
  },
  {
    nameOfPrize: 'PZ-50',
    pattern: 'A',
    quantity: 1,
    prizeAmount: 50
  }
]

export const gameTicketMock = [
  {
    orientation: 'HORIZONTAL',
    measurementUnit: 'CM',
    width: 10.2,
    height: 6.1,
    numberOfScratchAreas: 2,
    frontValidationPattern: 'RR-VVVVVV-C',
    winnerRedemptionPattern: 'GGGG-PPP-TTTT-RRRRR-VVVVVVVVVV-X'
  }
]

export const gameOtherDefinitionsMock = {
  poolLifecycle: {
    ticketDestruction: '2023-10-15',
    gameActivation: {
      from: '2023-06-15',
      to: '2023-09-15'
    },
    distribution: {
      from: '2023-06-15',
      to: '2023-09-15'
    },
    sales: {
      from: '2023-06-15',
      to: '2023-09-15'
    },
    validation: {
      from: '2023-06-15',
      to: '2023-09-15'
    },
    endReturningInventory: '2023-09-15',
    enableInitialDistribution: false
  },
  settlements: {
    afterPackReception: 15,
    afterPackActivation: 5,
    enableSettlementAnotherPackActivated: true
  },
  commissions: {
    sales: 10.75,
    prizePayment: 6
  }
}

export const gameReportsGameSalesMock = [
  {
    gameId: 'e4c62f76',
    gameName: 'game test 002',
    lotteryNumber: '567',
    averageSalesByWeek: 14.754498332998782,
    averageSalesByMonth: 76.39124293879144,
    averageSalesByDayByRetailer: 38.56238404993623,
    averageValidationsPerWeek: 45.29761641811798,
    daysInActiveStatus: 24
  },
  {
    gameId: '7e00d3a3',
    gameName: 'game test 001',
    lotteryNumber: '002',
    averageSalesByWeek: 70.51853613552016,
    averageSalesByMonth: 119.93809446092101,
    averageSalesByDayByRetailer: 18.639841852169297,
    averageValidationsPerWeek: 119.25623496265824,
    daysInActiveStatus: 67
  },
  {
    gameId: '20a09c0a',
    gameName: 'game test 003',
    lotteryNumber: '56789',
    averageSalesByWeek: 105.58117260259998,
    averageSalesByMonth: 119.98630389686221,
    averageSalesByDayByRetailer: 84.27757477398502,
    averageValidationsPerWeek: 27.502130645600793,
    daysInActiveStatus: 15
  },
  {
    gameId: '57324dbb',
    gameName: 'game test 010 david test',
    lotteryNumber: '44567',
    averageSalesByWeek: 47.49520631218165,
    averageSalesByMonth: 36.09646251641662,
    averageSalesByDayByRetailer: 129.64078255270465,
    averageValidationsPerWeek: 105.54607701234272,
    daysInActiveStatus: 56
  },
  {
    gameId: 'bb1d5c5f',
    gameName: 'test sp23 ',
    lotteryNumber: '332551',
    averageSalesByWeek: 27.95428004281492,
    averageSalesByMonth: 68.6662454206162,
    averageSalesByDayByRetailer: 54.93132158927938,
    averageValidationsPerWeek: 111.89851754957331,
    daysInActiveStatus: 17
  },
  {
    gameId: '6846fbcf',
    gameName: 'regression sp22 test 1',
    lotteryNumber: '454545',
    averageSalesByWeek: 74.76636150440183,
    averageSalesByMonth: 26.32751382342123,
    averageSalesByDayByRetailer: 65.19696436350448,
    averageValidationsPerWeek: 20.793142507125438,
    daysInActiveStatus: 69
  },
  {
    gameId: '6768f7dc',
    gameName: 'regression sp22 test 2',
    lotteryNumber: '4545456',
    averageSalesByWeek: 66.73350620396137,
    averageSalesByMonth: 104.60422817953143,
    averageSalesByDayByRetailer: 114.05056967659641,
    averageValidationsPerWeek: 117.3165390693617,
    daysInActiveStatus: 22
  },
  {
    gameId: '761aca28',
    gameName: 'regression sp22 test 3',
    lotteryNumber: '7987654687965',
    averageSalesByWeek: 11.378868923664335,
    averageSalesByMonth: 88.40462036216917,
    averageSalesByDayByRetailer: 83.42837148235243,
    averageValidationsPerWeek: 79.15614392900906,
    daysInActiveStatus: 37
  },
  {
    gameId: 'ecf49fe8',
    gameName: 'regression sp22 test4',
    lotteryNumber: '658479687',
    averageSalesByWeek: 59.87344321227271,
    averageSalesByMonth: 97.73126639763113,
    averageSalesByDayByRetailer: 113.29652539277558,
    averageValidationsPerWeek: 123.76284641353897,
    daysInActiveStatus: 25
  }
]

export const gameReportsClosedGamesMock = [
  {
    gameId: 'e4c62f76',
    gameName: 'game test 002',
    lotteryNumber: '567',
    startedAt: '2023-10-14T12:28:24.169838',
    closedAt: '2023-10-25T12:28:24.759407',
    daysActive: 11,
    percentageInPrizesEstimated: 13.04486,
    percentageInPrizesPaid: 11.18768,
    soldTickets: 134134,
    nonSoldTickets: 188
  },
  {
    gameId: '7e00d3a3',
    gameName: 'game test 001',
    lotteryNumber: '002',
    startedAt: '2023-08-21T12:28:24.169838',
    closedAt: '2023-10-11T12:28:24.759407',
    daysActive: 56,
    percentageInPrizesEstimated: 45.10864,
    percentageInPrizesPaid: 45.054,
    soldTickets: 1199118,
    nonSoldTickets: 982
  },
  {
    gameId: '20a09c0a',
    gameName: 'game test 003',
    lotteryNumber: '56789',
    startedAt: '2023-08-14T12:28:24.169838',
    closedAt: '2023-10-12T12:28:24.759407',
    daysActive: 42,
    percentageInPrizesEstimated: 39.0546,
    percentageInPrizesPaid: 37.05648,
    soldTickets: 1510684,
    nonSoldTickets: 18610
  },
  {
    gameId: '57324dbb',
    gameName: 'game test 010 david test',
    lotteryNumber: '44567',
    startedAt: '2023-07-01T12:28:24.169838',
    closedAt: '2023-10-09T12:28:24.759407',
    daysActive: 74,
    percentageInPrizesEstimated: 54.0541,
    percentageInPrizesPaid: 51.051531,
    soldTickets: 2016486,
    nonSoldTickets: 8711
  },
  {
    gameId: 'bb1d5c5f',
    gameName: 'test sp23 ',
    lotteryNumber: '332551',
    startedAt: '2023-09-11T12:28:24.169838',
    closedAt: '2023-10-23T12:28:24.759407',
    daysActive: 32,
    percentageInPrizesEstimated: 61.054534,
    percentageInPrizesPaid: 56.1864,
    soldTickets: 2180081,
    nonSoldTickets: 118648
  },
  {
    gameId: '6846fbcf',
    gameName: 'regression sp22 test 1',
    lotteryNumber: '454545',
    startedAt: '2023-10-15T12:28:24.169838',
    closedAt: '2023-10-25T12:28:24.759407',
    daysActive: 10,
    percentageInPrizesEstimated: 13.04486,
    percentageInPrizesPaid: 11.18768,
    soldTickets: 134134,
    nonSoldTickets: 188
  }
]
