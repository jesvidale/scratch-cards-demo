import { retailerBasicInfo, retailerStatusModel } from '~/models/retailer'

const allRetailersMock = [
  {
    hasMissingInfo: false,
    retailerName: 'Ronny Retailer',
    address: 'Baker Street 221B',
    retailerNumber: '4711',
    retailerId: '1',
    courier: 'NORTH',
    area: 'DHL',
    telSales: 'Angelica',
    status: 'ACTIVE'
  },
  {
    hasMissingInfo: true,
    retailerName: 'Richard Retailer',
    address: 'Oxford Street 12',
    retailerNumber: '9988',
    retailerId: '2',
    courier: 'SOUTHWEST',
    area: 'UPS',
    telSales: 'Quentin',
    status: 'DEACTIVATED'
  },
  {
    hasMissingInfo: false,
    retailerName: 'Mills and sons',
    address: '1234 Abbie Highway',
    retailerNumber: '23909',
    retailerId: '3',
    courier: 'NORTHWEST',
    area: 'DHL',
    telSales: 'Catherine',
    status: 'ACTIVE'
  },
  {
    hasMissingInfo: false,
    retailerName: 'Homenick Inc',
    address: 'Häuschen strasse 12',
    retailerNumber: '59871',
    retailerId: '4',
    courier: 'SOUTH',
    area: 'Correos',
    telSales: 'Irina',
    status: 'ACTIVE'
  }
]

export const getAllRetailersMock = (status: retailerStatusModel) => {
  if (status === 'ALL') { return allRetailersMock }
  return allRetailersMock.filter((retailer: retailerBasicInfo) => retailer.status === status)
}

export const allAreasMock = ['NORTH', 'NORTHWEST', 'WEST', 'SOUTHWEST', 'SOUTH', 'SOUTHEAST', 'EAST', 'NORTHEAST']

export const allTelSalesMock = [
  {
    name: 'Angelica',
    id: '004rw'
  },
  {
    name: 'Quentin',
    id: '047sr'
  },
  {
    name: 'Catherine',
    id: '521tr'
  },
  {
    name: 'Irina',
    id: '778wm'
  }
]

export const allFieldRepresentativeMock = [
  {
    name: 'Field representative 1',
    id: 'h15rw'
  },
  {
    name: 'Field representative 2',
    id: 'h16rw'
  },
  {
    name: 'Field representative 3',
    id: 'h17rw'
  },
  {
    name: 'Field representative 4',
    id: 'h18rw'
  },
  {
    name: 'Field representative 5',
    id: 'h19rw'
  }
]

export const allCourierMock = [
  {
    name: 'Courier 1',
    id: 'd15sl'
  },
  {
    name: 'Courier 2',
    id: 'd16sl'
  },
  {
    name: 'Courier 3',
    id: 'd17sl'
  },
  {
    name: 'Courier 4',
    id: 'd18sl'
  },
  {
    name: 'Courier 5',
    id: 'd19sl'
  }
]

const retailerMasterDataMock = [
  {
    retailerId: '1',
    retailerName: 'Ronny Retailer',
    retailerNumber: '4711',
    address: 'Baker Street 221B',
    zipCode: '48574',
    city: 'Goldabury',
    telSale: 'Angelica Müller',
    area: 'NORTH',
    settlement: {
      afterPackActivation: 13,
      afterPackReception: 15,
      enableSettlementAnotherPackActivated: true
    },
    limits: {
      packsOrder: 450,
      inventory: 480,
      balance: 1200500
    },
    status: 'ACTIVE'
  },
  {
    retailerId: '2',
    retailerName: 'Richard Retailer',
    retailerNumber: '9988',
    address: 'Oxford Street 12',
    zipCode: null,
    city: null,
    telSale: 'Quentin',
    area: 'SOUTHWEST',
    settlement: {
      afterPackActivation: null,
      afterPackReception: null,
      enableSettlementAnotherPackActivated: false
    },
    limits: {
      packsOrder: null,
      inventory: null,
      balance: null
    },
    status: 'DEACTIVATED'
  },
  {
    retailerId: '3',
    retailerName: 'Mills and sons',
    retailerNumber: '23909',
    address: '1234 Abbie Highway',
    zipCode: '48574',
    city: 'Frankfurt',
    telSale: 'Catherine',
    area: 'NORTHWEST',
    settlement: {
      afterPackActivation: 22,
      afterPackReception: 12,
      enableSettlementAnotherPackActivated: true
    },
    limits: {
      packsOrder: 420,
      inventory: 520,
      balance: 1102000
    },
    status: 'ACTIVE'
  },
  {
    retailerId: '4',
    retailerName: 'Homenick Inc',
    retailerNumber: '59871',
    address: 'Häuschen strasse 12',
    zipCode: '48574',
    city: 'Stuttgart',
    telSale: 'Irina23',
    area: 'SOUTH',
    settlement: {
      afterPackActivation: 10,
      afterPackReception: 12,
      enableSettlementAnotherPackActivated: true
    },
    limits: {
      packsOrder: 450,
      inventory: 480,
      balance: 1200500
    },
    status: 'ACTIVE'
  }
]

export const getRetailerMasterDataMock = (retailerId: string | string[]) => retailerMasterDataMock.find((retailer: object) => retailer.retailerId === retailerId)

const retailerOtherDataMock = [
  {
    retailerId: '1',
    contactName: 'Hane group',
    phoneNumber: '66113668',
    email: 'hanegroup@info.com',
    fieldRepresentative: 'Field representative 1',
    courier: 'Courier 1',
    specialBalanceCadence: 10,
    specificInvoiceCadence: false,
    specificLimitPayablePrize: 5,
    initialDistribution: true,
    numberOfPacks: 20,
    requiredReturnPaidTickets: false,
    commissionsPerGame: [{
      gameId: '219d3fc2',
      sales: 5,
      prizePayment: 10
    },
    {
      gameId: '162e21ab',
      sales: 15,
      prizePayment: 8
    },
    {
      gameId: 'c70be0b7',
      sales: 8,
      prizePayment: 20
    }]
  },
  {
    retailerId: '3',
    contactName: 'Mils and sons',
    phoneNumber: '75223378',
    email: 'milifacuo@milssons.de',
    fieldRepresentative: 'Field representative 3',
    courier: 'Courier 4',
    specialBalanceCadence: 4,
    specificInvoiceCadence: true,
    specificLimitPayablePrize: 10,
    initialDistribution: true,
    numberOfPacks: 20,
    requiredReturnPaidTickets: false,
    commissionsPerGame: [{
      gameId: '162e21ab',
      sales: 10,
      prizePayment: 12
    },
    {
      gameId: 'ef35910a',
      sales: 10,
      prizePayment: 14
    }]
  }
]

export const getRetailerOtherDataMock = (retailerId: string | string[]) => retailerOtherDataMock.find((retailer: object) => retailer.retailerId === retailerId)
