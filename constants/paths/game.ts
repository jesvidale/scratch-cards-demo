export const GAME_DASHBOARD = '/game/dashboard'

export const GAME_CREATE = '/game/create'

export const GAME_LOAD = '/game/load'
export const GAME_LOAD_SLUG_PRIZE = 'prize-structure'
export const GAME_LOAD_SLUG_DESIGN = 'ticket-design'
export const GAME_LOAD_SLUG_GLTP = 'gltp'
export const GAME_LOAD_SLUG_DATES = 'other-definitions'
export const GAME_LOAD_SLUG_REVIEW = 'review-load'

export const GAME_REPORTS = '/game/reports'
export const GAME_REPORTS_SLUG_LOAD_FILES = 'load-files'

export const gameLoadRoute = ({ route, id }:{route:string, id?: string | string[]}) =>
  `${GAME_LOAD}${id && '/' + id}${route && '/' + route}`

export const gameReportRoute = ({ route, id }:{route:string, id?: string | string[]}) =>
  `${GAME_REPORTS}${id && '/' + id}${route && '/' + route}`
