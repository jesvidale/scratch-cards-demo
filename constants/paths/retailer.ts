export const RETAILER_DASHBOARD = '/retailers/dashboard'
export const RETAILER_CREATE = '/retailers/create'
export const RETAILER_SETTLEMENT = '/retailers/settlement-rules'
export const RETAILER_COMMISSIONS = '/retailers/commissions'

export const RETAILER_DETAIL_SLUG_MASTER_DATA = 'master-data'
export const RETAILER_DETAIL_SLUG_OTHER_DATA = 'other-data'

export const editRetailer = ({ route, id }:{route:string, id?: string | string[]}) => '/retailers/' + id + '/detail/' + route
