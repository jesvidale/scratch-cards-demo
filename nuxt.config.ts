// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  telemetry: false,
  runtimeConfig: {
    // The private keys which are only available server-side
    apiSecret: '123',
    // Keys within public are also exposed client-side
    public: {
      useMockedData: true, // fill the response with mocks in development mode when the service fails or doesn't exist in backend
      gameServiceUrl: 'https://rzdev-allinone-ora-g1.dmz.tqg-hh.local/gameservice',
      logisticsServiceUrl: 'https://rzdev-allinone-ora-g1.dmz.tqg-hh.local/logisticsservice',
      orderServiceUrl: 'https://rzdev-allinone-ora-g1.dmz.tqg-hh.local/orderservice',
      retailerServiceUrl: 'https://rzdev-allinone-ora-g1.dmz.tqg-hh.local/retailerservice'
    }
  },
  css: ['assets/styles/main.scss'],
  modules: [
    'nuxt-quasar-ui'
  ],
  quasar: {
    extras: {
      font: 'roboto-font'
    },
    components: {
      defaults: {
        QBtn: {
          unelevated: true
        },
        QCard: {
          flat: true
        }
      }
    }
  },
  vite: {
    define: {
      'process.env.DEBUG': false
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/styles/_theme.scss" as *;'
        }
      }
    }
  },
  components: [
    {
      path: '~/components',
      pathPrefix: false
    }
  ]
})
