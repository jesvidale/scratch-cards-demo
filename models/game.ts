export type gameStatus = 'CREATED' | 'ACTIVE' | 'CANCELED' | 'LOADED' | 'IN_PAUSE' | 'UPDATING'| 'CLOSED'

export interface gameDescription {
  lotteryNumber?: string
  name?: string
  startDate?: string
  numberOfTickets?: string
}

export interface gameInformation {
  systemGameId: string
  status: gameStatus[]
  name: string
  lotteryName: string
  lotteryNumber: string
  startDate: string
  isActive: boolean
  creationDate: string
  creationUser: string
}

export interface loadGameDefinition {
  numberOfPools?: number,
  numberOfTicketsByPool?: number
  numberOfPacksByCarton?: number
  numberOfTicketsByPack?: number
  packWeight?: number
  packUnitOfWeight?: string
  ticketPrize?: number
  percentageDedicatedToPrizes?: number
  gameSize?: number
  prizeFund?: number
  odds: string
}

interface loadGamePrizeFundItem {
  category?: 'LOW' | 'MID' | 'HIGH'
  tierSequence?: number
  prizeType?: string
  tierValidationCode?: string
  prizeAmount?: number
  prizesPerPack?: number
  totalOfWinners?: number
  totalPrizeAmount?: number
}

export interface loadGamePrizeFund extends Array<loadGamePrizeFundItem>{}

export interface loadGameTicketDesign {
  orientation?: string
  measurementUnit?: string
  width?: number
  height?: number
  numberOfScratchAreas?: 1 | 2 | 3
  frontValidationPattern?: string
  winnerRedemptionPattern?: string
}

interface loadGameGltpItem {
  nameOfPrize: string,
  pattern: string,
  quantity: 0,
  prizeAmount: 0
}

export interface loadGameGltp extends Array<loadGameGltpItem>{}
