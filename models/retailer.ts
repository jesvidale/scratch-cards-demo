export interface retailerDefinition {
  id?: string,
  retailerNo?: string
  name: string,
  address?: string,
  area: string,
  courier: string
}

export interface wheelOfCallAction {
  id: number,
  retailer: string,
  phone: string,
  dueDate: string,
}

export type retailerStatusModel = 'ALL' | 'ACTIVE' | 'DEACTIVATED'

type retailerStatus = 'ACTIVE' | 'DEACTIVATED'

export interface retailerFiltersModel {
  retailerName: string,
  retailerStatus: retailerStatusModel
}

export interface retailerMasterDataModel {
  retailerId: string,
  retailerName: string,
  retailerNumber: string,
  address: string,
  zipCode: string,
  city: string,
  telSale: string,
  area: string,
  settlement: {
    afterPackActivation: number,
    afterPackReception: number,
    enableSettlementAnotherPackActivated: boolean
  },
  limits: {
    packsOrder: number,
    inventory: number,
    balance: number
  },
  status: retailerStatus
}

export interface retailerBasicInfo {
  hasMissingInfo: boolean,
  retailerName: string,
  address: string,
  retailerNumber: string,
  retailerId: string,
  courier: string,
  area: string,
  telSales: string,
  status: retailerStatusModel
}
