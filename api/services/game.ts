import { gameStatus, loadGameDefinition, loadGamePrizeFund, loadGameTicketDesign } from 'models/game'
import { getGamesListMock, getGameMock, gameDefinitionMock, gameOtherDefinitionsMock, gameGltpMock, gamePrizeStructureMock, gameReportsClosedGamesMock, gameReportsGameSalesMock } from '~/mocks/game'
import { api } from '~/api/api'

export const gameService = {
  games: {
    getAll: async (params: { status: gameStatus[] }) => {
      const allStatus = ['CREATED', 'ACTIVE', 'LOADED', 'ENDED', 'CLOSED']
      const stringifiedParams = !params.status.length ? allStatus.toString() : params.status.toString()
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.gameServiceUrl + '/v1/games',
        params: { status: stringifiedParams },
        mock: getGamesListMock(params.status)
      })
      return { pending, data, error }
    },
    get: async (gameId: string | string[]) => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.gameServiceUrl + '/v1/games/' + gameId,
        mock: getGameMock(gameId)
      })
      return { pending, data, error }
    },
    create: async (params: object) => {
      const { pending, data, error } = await api.post(
        useRuntimeConfig().public.gameServiceUrl + '/v1/games',
        params
      )
      return { pending, data, error }
    },
    activate: async (gameId: string | string[]) => {
      const { pending, data, error } = await api.post(
        useRuntimeConfig().public.gameServiceUrl + '/v1/games/activate/' + gameId,
        {}
      )
      return { pending, data, error }
    },
    end: async (gameId: string | string[]) => {
      const { pending, data, error } = await api.post(
        useRuntimeConfig().public.gameServiceUrl + '/v1/games/end/' + gameId,
        {}
      )
      return { pending, data, error }
    },
    close: async (gameId: string | string[]) => {
      const { pending, data, error } = await api.post(
        useRuntimeConfig().public.gameServiceUrl + '/v1/games/close/' + gameId,
        {}
      )
      return { pending, data, error }
    }
  },
  definition: {
    get: async (gameId: string | string[]) => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.gameServiceUrl + '/v1/games/' + gameId + '/definition',
        mock: gameDefinitionMock
      })
      return { pending, data, error }
    },
    put: async (gameId: string, params: loadGameDefinition) => {
      const { pending, data, error } = await api.put(
        useRuntimeConfig().public.gameServiceUrl + '/v1/games/' + gameId + '/definition',
        params
      )
      return { pending, data, error }
    }
  },
  reports: {
    gameSales: {
      get: async () => {
        const { pending, data, error } = await api.get({
          url: useRuntimeConfig().public.gameServiceUrl + '/v1/games/game-sales-report',
          mock: gameReportsGameSalesMock
        })
        return { pending, data, error }
      }
    },
    closedGames: {
      get: async () => {
        const { pending, data, error } = await api.get({
          url: useRuntimeConfig().public.gameServiceUrl + '/v1/games/closed-games-report',
          mock: gameReportsClosedGamesMock
        })
        return { pending, data, error }
      }
    }
  }
}
