import { retailerStatusModel, retailerMasterDataModel } from 'models/retailer'
import { allAreasMock, getAllRetailersMock, allTelSalesMock, getRetailerMasterDataMock, allCourierMock, allFieldRepresentativeMock, getRetailerOtherDataMock } from '~/mocks/retailers'
import { api } from '~/api/api'

export const retailerService = {
  retailers: {
    getAll: async (params: { status: retailerStatusModel }) => {
      const allStatus = ['ACTIVE', 'DEACTIVATED']
      const stringifiedParams = params?.status === 'ALL' ? allStatus.toString() : params?.status.toString()
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers',
        params: { status: stringifiedParams },
        mock: getAllRetailersMock(params.status)
      })
      return { pending, data, error }
    },
    masterData: {
      get: async (retailerId: string | string[]) => {
        const { pending, data, error } = await api.get({
          url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/' + retailerId,
          mock: getRetailerMasterDataMock(retailerId)
        })
        return { pending, data, error }
      },
      put: async (retailerId: string, params: retailerMasterDataModel) => {
        const { pending, data, error } = await api.put(
          useRuntimeConfig().public.gameServiceUrl + '/v1/retailers/' + retailerId,
          params
        )
        return { pending, data, error }
      }
    },
    otherData: {
      get: async (retailerId: string | string[]) => {
        const { pending, data, error } = await api.get({
          url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/' + retailerId + '/other-data',
          mock: getRetailerOtherDataMock(retailerId)
        })
        return { pending, data, error }
      },
      put: async (retailerId: string, params: retailerMasterDataModel) => {
        const { pending, data, error } = await api.put(
          useRuntimeConfig().public.gameServiceUrl + '/v1/retailers/' + retailerId + '/other-data',
          params
        )
        return { pending, data, error }
      }
    }
  },
  areas: {
    get: async () => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/areas',
        mock: allAreasMock
      })
      return { pending, data, error }
    }
  },
  telSales: {
    get: async () => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/telsales',
        mock: allTelSalesMock
      })
      return { pending, data, error }
    }
  },
  fieldRepresentative: {
    get: async () => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/field-representative',
        mock: allFieldRepresentativeMock
      })
      return { pending, data, error }
    }
  },
  courier: {
    get: async () => {
      const { pending, data, error } = await api.get({
        url: useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/courier',
        mock: allCourierMock
      })
      return { pending, data, error }
    }
  },
  settlement: {
    put: async (retailerId: string, params: object) => {
      const { pending, data, error } = await api.put(
        useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/' + retailerId + '/settlement-rules',
        params
      )
      return { pending, data, error }
    }
  },
  commissions: {
    put: async (retailerId: string, params: object) => {
      const { pending, data, error } = await api.put(
        useRuntimeConfig().public.retailerServiceUrl + '/v1/retailers/' + retailerId + '/commissions',
        params
      )
      return { pending, data, error }
    }
  }
}
