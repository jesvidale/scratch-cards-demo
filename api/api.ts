import { UseFetchOptions } from 'nuxt/app'
import { KeysOf } from 'nuxt/dist/app/composables/asyncData'

const myHeaders = new Headers()
myHeaders.append('Content-Type', 'application/json')

type IRequestOptions = UseFetchOptions<unknown, unknown, KeysOf<unknown>, null, string, 'get' | 'head' | 'patch' | 'post' | 'put' | 'delete' | 'connect' | 'options' | 'trace'>

export const api = {
  get: async ({ url, params = {}, mock }: {url: string, params?: object, mock?: object | []}) => {
    const { pending, data, error } = await useFetch(
      url,
      {
        method: 'GET',
        params
      }
    )
    if (error.value && process.env.NODE_ENV === 'development' && useRuntimeConfig().public.useMockedData) {
      pending.value = false
      data.value = mock
      error.value = null
    }
    return { pending, data, error }
  },
  post: async (url: string, raw: object) => {
    const requestOptions:IRequestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: 'follow'
    }
    const { pending, data, error } = await useFetch(
      url,
      requestOptions
    )
    return { pending, data, error }
  },
  postFile: async (url: string, files: object) => {
    const fileHeaders = new Headers()
    const requestOptions:IRequestOptions = {
      method: 'POST',
      headers: fileHeaders,
      body: files,
      redirect: 'follow'
    }
    const { pending, data, error } = await useFetch(
      url,
      requestOptions
    )
    return { pending, data, error }
  },
  put: async (url: string, raw: object) => {
    const requestOptions:IRequestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: 'follow'
    }
    const { pending, data, error } = await useFetch(
      url,
      requestOptions
    )
    return { pending, data, error }
  },
  patch: async (url: string, raw: object) => {
    const requestOptions:IRequestOptions = {
      method: 'PATCH',
      headers: myHeaders,
      body: JSON.stringify(raw),
      redirect: 'follow'
    }
    const { pending, data, error } = await useFetch(
      url,
      requestOptions
    )
    return { pending, data, error }
  }
}
