FROM nexus.businessengineers.de/library/node:18.16.0-bookworm

RUN mkdir -p /usr/instants-frontend
WORKDIR /usr/instants-frontend

COPY ./.output /usr/instants-frontend

EXPOSE 3000
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD ["node", "server/index.mjs"]