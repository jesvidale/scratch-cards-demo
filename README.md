# Scratch cards (demo)


The objective of this project is to make a presentation as a portfolio on the management of Vue 3 with typescript and Vite. As this is a demo, there are many project functionalities that are incomplete or eliminated.

## Product explanation:

The project is about scratch cards. There are two functionalities in the demo: Game and Retailers

### GAME:

Set of scratch cards grouped by theme and with a start - end date.

Parts:

- Dashboard: A list of existing games with their status within their life cycle is displayed by default. This listing is filterable. Depending on the state of the game, certain actions or others can be executed.

- Reports: Metris for games

### RETAILERS:

The person, shop, or business that sells scratch cards to the public.

Parts:

- Dashboard: A list of existing retailers filterable by name and status.

- Detail: Detail for each retailer. It involves the basic information like address and contact, and some more advanced related to game rules preferences.

- Settlement and commissions: Rules for each retailer applied globally or per game.


## Setup

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

### Development Server

Start the development server on `http://localhost:3000`

```bash
npm run dev
```

### Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
